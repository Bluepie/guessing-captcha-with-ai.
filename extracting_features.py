import os
import cv2
import glob
import pandas as pd
import numpy as np
import sys
import imutils

#helderFuntions
def displayImage(image):
	cv2.imshow('image',image)
	cv2.waitKey(0);
	cv2.destroyAllWindows()
	exit()

def displayContours(img,cont):
		cv2.drawContours(img, cont, -1, (0,255,0), 3)
		cv2.imshow("title", img)
		cv2.waitKey()

counts = {}

#readallimages
images = glob.glob(os.path.join('ds/train', "*"))
#reading data
datainimages = pd.read_csv('ds/trainLabels.csv',names=['index','nameValues'],header=None)
#reset index with a specfic colummn as index
datainimages.set_index('index',inplace=True)
#converting data to dictoonary,key value pairs.
datainimages = datainimages.to_dict('index')
for (index,eachimage) in enumerate(images):
	image = cv2.imread(eachimage)
	#geting the number from file name
	nameFile = eachimage.split('/')[-1]
	#original name from number 
	originalName = datainimages.get(int(nameFile))['nameValues']
	#convert to grey scale
	greySclaed = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
	#testing with image show
	(thresh, im_bw) = cv2.threshold(greySclaed, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)
	contours, hierarchy = cv2.findContours(im_bw, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

	letter_image_regions = []
	for contour in contours:
		(x, y, w, h) = cv2.boundingRect(contour)
		if w / h > 1.5:
			half_width = int(w / 2)
			letter_image_regions.append((x, y, half_width, h))
			letter_image_regions.append((x + half_width, y, half_width, h))
		else:
			letter_image_regions.append((x, y, w, h))

			letter_image_regions = sorted(letter_image_regions, key=lambda x: x[0])

	if len(letter_image_regions) != 6:
		continue

	for letter_bounding_box, letter_text in zip(letter_image_regions, originalName):
		# Grab the coordinates of the letter in the image
		x, y, w, h = letter_bounding_box

		# Extract the letter from the original image with a 2-pixel margin around the edge
		letter_image = greySclaed[y - 2:y + h + 2, x - 2:x + w + 2]


		# Get the folder to save the image in
		save_path = os.path.join('extracted_letters', letter_text)

		# if the output directory does not exist, create it
		if not os.path.exists(save_path):
			os.makedirs(save_path)

		# write the letter image to a file
		count = counts.get(letter_text, 1)
		p = os.path.join(save_path, "{}.png".format(str(count).zfill(6)))
		cv2.imwrite(p, letter_image)

		# increment the count for the current key
		counts[letter_text] = count + 1
